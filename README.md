```mermaid
graph LR;
    A(Received message) --> B{Message<br>Number};
    B-->|6|msg6{Destination MMSI<br> = Own station};
    B-->|25|msg25{Unstructured<br>type};
    B-->|other|NonConfig[Non configuraiton<br>process];
    msg6-->|Yes|DAC{DAC = 990?};
    msg6-->|No|Dest{Destination = 0 &&<br>source MMSI = parent};
    Dest-->|Yes|DAC;
    Dest-->|No|Ignore[Ignore<br>message];
    DAC-->|No|NonConfig;
    DAC-->|Yes|DecMsg[Decrypt<br>message];
    msg25-->|No|NonConfig;
    msg25-->|Yes|DecMsg;
```